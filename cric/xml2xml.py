from bs4 import BeautifulSoup
from xml.dom.minidom import Document
import unicodedata

#set the arrays of required attributes
#Matchdetail_attrs = ['Awayteam','Awayteam_Id','Event','Hometeam','Hometeam_Id','LastUpdateddate','LastUpdatedtime','Match_Id','Venue','currentinning']
Matchdetail_attrs = {
					'Awayteam':'AwTm',
					'Awayteam_Id': 'AwTmId' ,
					'Event': 'Evt',
					'Hometeam':'HmTm',
					'Hometeam_Id':'HmTmId',
					'LastUpdateddate':'LsUpDt',
					'LastUpdatedtime':'LsUpTm',
					'Match_Id':'MchId',
					'Venue': 'Vnu' ,
					}

#Currentinnings_attrs = ['AllottedOvers','Battingteam','Battingteam_Id','Bowlingteam','Bowlingteam_Id','Target']
Currentinnings_attrs = {
					'AllottedOvers':'AltdOvs' ,
					'Battingteam' : 'BtTm',
					'Battingteam_Id':'BtTmId' ,
					'Bowlingteam':'BwTm' ,
					'Bowlingteam_Id':'BwTmId' ,
					'Target': 'Trg',
					}

#FOIEquation_attrs = ['Bowlersused','Overs','Runrate','Total','Wickets']
FOIEquation_attrs = {
					'Overs': 'Ovs' ,
					'Runrate':'RR' ,
					'Total': 'Tot',
					'Wickets': 'Wkts',
					}

#batsman_attrs=['BallsFaced','Batsman_Id','Runs']
batsman_attrs={
					'BallsFaced':'Bls' ,
					'Batsman_Id':'BtId' ,
					'Runs': 'Rn',
					}

soup = BeautifulSoup(open('recaserverxmlparsing/winzt17252012.xml'),'xml')

matchdetail = soup.findAll('Matchdetail')

current_inning_prefix = {
	'First' : 'FI',
	'Second': 'SI',
	'Third' : 'TI',
	'Fourth': 'FOI',
}
current_inning_key = {
	'First' : 'FirstInnings',
	'Second': 'SecondInnings',
	'Third' : 'ThirdInnings',
	'Fourth': 'FourthInnings',
}

batsmen = soup.findAll(current_inning_prefix[matchdetail[0]['currentinning']]+'Batsman',Howout='not out')
soup.findAll(Bowling="Yes")

#create xml document structure
doc = Document()
Matchdetail = doc.createElement("MchDtl")
doc.appendChild(Matchdetail)
for key in Matchdetail_attrs:
	Matchdetail.setAttribute(Matchdetail_attrs[key],matchdetail[0][key])

currentinning = soup.findAll(current_inning_key[matchdetail[0]['currentinning']])
Currentinnings = doc.createElement("CrtInns")
Matchdetail.appendChild(Currentinnings)
for key in Currentinnings_attrs:
	Currentinnings.setAttribute(Currentinnings_attrs[key],currentinning[0][key])

foiequation = soup.findAll('FOIEquation')
FOIEquation = doc.createElement("Det")
Currentinnings.appendChild(FOIEquation)
for key in FOIEquation_attrs:
	FOIEquation.setAttribute(FOIEquation_attrs[key],foiequation[0][key])

Currentbatsmen = doc.createElement("Bats")
Currentinnings.appendChild(Currentbatsmen)

for batsman in batsmen:
	Currentbatsman = doc.createElement('Bat')
	for key in batsman_attrs:
		Currentbatsman.setAttribute(batsman_attrs[key],batsman[key])
	Currentbatsmen.appendChild(Currentbatsman)
	Currentbatsman.appendChild(doc.createTextNode(batsman.contents[0].encode('ascii','ignore')))

Currentbowlers = doc.createElement("Bwls")
Currentinnings.appendChild(Currentbowlers)
Currentbowlers.appendChild(doc.createTextNode(''))

try:
	f=open('score.xml','r+')
except:
	f=open('score.xml','w')
f.write(doc.toprettyxml(indent="  "))
f.close()
#print doc.toprettyxml(indent="  ")