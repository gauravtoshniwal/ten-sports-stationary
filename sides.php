<?php
function SaveToFile() {
    if (isset($_POST["image_data"]))
    {
        // Get the data
        $imageData=$_POST['image_data'];
        $empId = $_POST['emp'];
        // Remove the headers (data:,) part.  
        // A real application should use them according to needs such as to check image type
        $filteredData=substr($imageData, strpos($imageData, ",")+1);
     
        // Need to decode before saving since the data we received is already base64 encoded
        $unencodedData=base64_decode($filteredData);
     
        //echo "unencodedData".$unencodedData;
     
        // Save file.  This example uses a hard coded filename for testing, 
        // but a real application can specify filename in POST variable
        if(file_exists("cards/".$empId.".png")) unlink("cards/".$empId.".png");
        //move_uploaded_file($fileTmpLoc, "documenti/$fileName");
        $fp = fopen( "cards/".$empId.'.png', 'wb' );
        if (fwrite( $fp, $unencodedData)) {
            echo "Upload Successful";
        }
        else {
            echo "Upload Failed";
        }
        fclose( $fp );
        //echo "Something";
    }
}

if(function_exists($_GET['f'])) {
   $_GET['f']();
}
?>